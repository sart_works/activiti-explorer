/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
'use strict';

var KISBPM = KISBPM || {};
KISBPM.USERFORM_PROPERTIES =
{
    "string": {
        "showUserFormProperties": "true"
    }
}

KISBPM.USERFORM_CONTROLS = [
  {
    "key": "input text",
    "type": "input",
    "templateOptions": {
      "label": "Text",
      "placeholder": "Type here to see the other field become enabled..."
    }
  },
  {
    key: 'textarea',
    type: 'textarea',
    templateOptions: {
      label: 'Some sweet story',
      placeholder: 'It allows you to build and maintain your forms with the ease of JavaScript :-)',
      description: ''
    }        
  },    
    {
        key: 'select marvel1',
        type: 'select',
        templateOptions: {
          label: 'Normal Select',
          options: [
            {name: 'Iron Man', value: 'iron_man'},
            {name: 'Captain America', value: 'captain_america'},
            {name: 'Black Widow', value: 'black_widow'},
            {name: 'Hulk', value: 'hulk'},
            {name: 'Captain Marvel', value: 'captain_marvel'}
          ]
        }
      },
    {
        key: 'select marvel2',
        type: 'select',
        templateOptions: {
          label: 'Grouped Select',
          options: [
            {name: 'Iron Man', value: 'iron_man', group: 'Male'},
            {name: 'Captain America', value: 'captain_america', group: 'Male'},
            {name: 'Black Widow', value: 'black_widow', group: 'Female'},
            {name: 'Hulk', value: 'hulk', group: 'Male'},
            {name: 'Captain Marvel', value: 'captain_marvel', group: 'Female'}
          ]
        }
      },
      {
        key: 'select marvel3',
        type: 'select',
        templateOptions: {
          label: 'Select with custom name/value/group',
          options: [
            {label: 'Iron Man', id: 'iron_man', gender: 'Male'},
            {label: 'Captain America', id: 'captain_america', gender: 'Male'},
            {label: 'Black Widow', id: 'black_widow', gender: 'Female'},
            {label: 'Hulk', id: 'hulk', gender: 'Male'},
            {label: 'Captain Marvel', id: 'captain_marvel', gender: 'Female'}
          ],
          groupProp: 'gender',
          valueProp: 'id',
          labelProp: 'label'
        }
      },
      {
        key: 'select marvel4',
        type: 'select',
        templateOptions: {
          label: 'Custom ng-options',
          options: [
            {label: 'Iron Man', id: 'iron_man', gender: 'Male'},
            {label: 'Captain America', id: 'captain_america', gender: 'Male'},
            {label: 'Black Widow', id: 'black_widow', gender: 'Female'},
            {label: 'Hulk', id: 'hulk', gender: 'Male'},
            {label: 'Captain Marvel', id: 'captain_marvel', gender: 'Female'}
          ],
          ngOptions: 'option as option.label group by option.gender for option in to.options'
        }
      }
];


KISBPM.USERFORM_TEMPLATES = 
{
'#default (generated)': {
    "generate": "Generate me!"
  },
'jsonUserForm': [
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-xs-6',
            type: 'input',
            key: 'firstName',
            templateOptions: {
              label: 'First Name'
            }
          },
          {
            className: 'col-xs-6',
            type: 'input',
            key: 'lastName',
            templateOptions: {
              label: 'Last Name'
            },
            expressionProperties: {
              'templateOptions.disabled': '!model.firstName'
            }
          }
        ]
      },
      {
        className: 'section-label',
        template: '<hr /><div><strong>Address:</strong></div>'
      },
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-xs-6',
            type: 'input',
            key: 'street',
            templateOptions: {
              label: 'Street'
            }
          },
          {
            className: 'col-xs-3',
            type: 'input',
            key: 'cityName',
            templateOptions: {
              label: 'City'
            }
          },
          {
            className: 'col-xs-3',
            type: 'input',
            key: 'zip',
            templateOptions: {
              type: 'number',
              label: 'Zip',
              max: 99999,
              min: 0,
              pattern: '\\d{5}'
            }
          }
        ]
      },
      {
        template: '<hr />'
      },
      {
        type: 'input',
        key: 'otherInput',
        templateOptions: {
          label: 'Other Input'
        }
      },
      {
        type: 'checkbox',
        key: 'otherToo',
        templateOptions: {
          label: 'Other Checkbox'
        }
      }
 ],
    "sample2" : [
   {
      "className": "row",
      "fieldGroup": [
         {
            "className": "col-xs-6",
            "type": "input",
            "key": "firstName",
            "templateOptions": {
               "label": "First Name"
            }
         },
         {
            "className": "col-xs-6",
            "type": "input",
            "key": "lastName",
            "templateOptions": {
               "label": "Last Name"
            },
            "expressionProperties": {
               "templateOptions.disabled": "!model.firstName"
            }
         }
      ]
   },
   {
      "className": "section-label",
      "template": "<hr /><div><strong>Address:<\/strong><\/div>"
   },
   {
      "className": "row",
      "fieldGroup": [
         {
            "className": "col-xs-6",
            "type": "input",
            "key": "street",
            "templateOptions": {
               "label": "Street"
            }
         },
         {
            "className": "col-xs-3",
            "type": "input",
            "key": "cityName",
            "templateOptions": {
               "label": "City"
            }
         },
         {
            "className": "col-xs-3",
            "type": "input",
            "key": "zip",
            "templateOptions": {
               "type": "number",
               "label": "Zip",
               "max": 99999,
               "min": 0,
               "pattern": "\\d{5}"
            }
         }
      ]
   },
   {
      "template": "<hr />"
   },
   {
      "type": "input",
      "key": "otherInput",
      "templateOptions": {
         "label": "Other Input"
      }
   },
   {
      "type": "checkbox",
      "key": "otherToo",
      "templateOptions": {
         "label": "Other Checkbox"
      }
   }
]
};